from Game import GameShow,Showman
import sys
import random

class Deal:
    def __init__(self, doorsCount=3) -> None:
        self.show = GameShow(doorsCount)

    def ChooseADoor(self, doorIndex, swapAllowed) -> bool:
        anotherDoorIndex = Showman.SuggestAnotherDoor(self.show, doorIndex)
        if swapAllowed:
            doorIndex = anotherDoorIndex
        if self.show.IsCarInThisDoor(doorIndex):
            return True # won
        else:
            return False # lost

def Run(swapAllowed=False, doorsCount=3):
    deal = Deal(doorsCount)
    chosenDoorIndex = random.randint(0, doorsCount-1)
    return deal.ChooseADoor(chosenDoorIndex, swapAllowed)

def main():
    swapAllowed = False
    if len(sys.argv) > 1:
        swapAllowed = bool(sys.argv[1].lower().startswith("t"))

    doorsCount = 3
    if len(sys.argv) > 2:
        doorsCount = int(sys.argv[2])

    won = Run(swapAllowed, doorsCount)
    if won:
        print("You win a Car!")
    else:
        print("Sorry! you lost.")

if __name__ == "__main__":
    main()
