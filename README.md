# Monty Hall Problem
It's a paradox that looks so weird until you test it with your code. Take a look at [Monty Hall problem](https://en.wikipedia.org/wiki/Monty_Hall_problem).

## Game.py
Implements the showman and the game show logic.

## Deal.py
Runs the game. you can run it in this format:
```
python3 Deal.py [SwapAllowed as boolean (default: false)] [DoorsCount as int (default: 3)]
```
`SwapAllowed` determines if you accept the showman's offer.

## Iterate.py
Runs the Deal for several times and prints out the result.
```
python3 Iterate.py [LoopCount as int] [SwapAllowed] [DoorsCount]
```

## Examples

### Don't Accept their offer!
```
[~/monty]$ python3 Iterate.py 1000000 false 3
You won 33% of the games. (334201/1000000)
```

### Swap your choice:
```
[~/monty]$ python3 Iterate.py 1000000 true 3
You won 67% of the games. (666829/1000000)
```