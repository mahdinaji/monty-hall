import random


class GameShow:
    def __init__(self, doorsCount=3) -> None:
        self.DoorsCount = doorsCount
        self._doors = [Door() for i in range(doorsCount)]

        self._correctDoorIndex = random.randint(0, doorsCount - 1)
        randomDoor = self._doors[self._correctDoorIndex]
        randomDoor.SetHasTheCar(True)

    def IsCarInThisDoor(self, doorIndex) -> bool:
        if doorIndex > len(self._doors):
            return False
        return self._doors[doorIndex].HasTheCar()
    
    def GetTheCorrectDoorIndex(self) -> int:
        return self._correctDoorIndex


class Door:
    def __init__(self) -> None:
        self._hasTheCar = False

    def SetHasTheCar(self, newState) -> None:
        self._hasTheCar = newState

    def HasTheCar(self) -> bool:
        return self._hasTheCar


class Showman:
    # SuggestAnotherDoor returns the door that might have a car, while the rest of the doors (not chosen) had a goat
    def SuggestAnotherDoor(gameShow, firstChosenDoorIndex) -> int:
        if gameShow.IsCarInThisDoor(firstChosenDoorIndex):
            # showman has already lost the game
            # suggest an empty door
            return (
                firstChosenDoorIndex + random.randint(1, gameShow.DoorsCount - 1)
            ) % gameShow.DoorsCount
        
        # find the door that has the car and suggest it, because we need to open the rest of the doors and prove that there was a goat behind it
        suggestedDoor = gameShow.GetTheCorrectDoorIndex()
    
        return suggestedDoor
