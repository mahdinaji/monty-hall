import sys
from Deal import Run


def main():
    iterationCount = 10
    if len(sys.argv) > 1:
        iterationCount = int(sys.argv[1])

    swapAllowed = False
    if len(sys.argv) > 2:
        swapAllowed = bool(sys.argv[2].lower().startswith("t"))

    doorsCount = 3
    if len(sys.argv) > 3:
        doorsCount = int(sys.argv[3])
    
    wonCount = 0
    for i in range(iterationCount):
        if Run(swapAllowed, doorsCount):
            wonCount = wonCount + 1
    
    print("You won {}% of the games. ({}/{})".format(round(wonCount*100/iterationCount), wonCount, iterationCount))

if __name__ == "__main__":
    main()